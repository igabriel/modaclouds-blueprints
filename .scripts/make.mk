
.DEFAULT : __help__
.PHONY : __always__
.SUFFIXES :
.PRECIOUS :
.SECONDARY :
.INTERMEDIATE :
.DELETE_ON_ERROR :
.POSIX :


inputs := $(realpath .)
publish := $(abspath ./.publish)
outputs := $(abspath ./.outputs)
outputs_exists := $(abspath $(outputs)/.exists)


pdf_viewer := zathura
eps_viewer := zathura
svg_viewer := geeqie
png_viewer := geeqie


.PHONY : __help__
__help__ : __always__
	@ echo
	@ echo '== Generic targets =='
	@ for target in __help__ __generate__ __publish__ __clean__ ; do \
		echo " $(script) $${target}" ; done
	@ echo
	@ echo '== Edit artefact targets =='
	@ for target in $(edit_targets) ; do \
		echo " $(script) $${target}" ; done \
	| sort
	@ echo
	@ echo '== View artefact targets =='
	@ for target in \
			$(filter-out \
				$(foreach output_type, $(raster_output_artefact_types), \
					$(foreach output_size, $(raster_output_artefact_sizes), \
						%.$(output_size).$(output_type))) \
				, $(view_targets)) \
	; do \
		echo " $(script) $${target}" ; done \
	| sort
	@ echo
	@ echo '== View all artefact targets =='
	@ for target in $(view_targets) ; do \
		echo " $(script) $${target}" ; done \
	| sort
	@ echo


.PHONY : __generate__
__generate__ : $(outputs)/.outputs __always__
	$(info [xx] [make] $(@))
	test -e $(<)

.PHONY : __publish__
__publish__ : $(outputs)/.publish __always__
	$(info [xx] [make] $(@))
	test -e $(<)
	test -e $(publish)
	rsync -a -v $(<)/ $(publish)/

.PHONY : __clean__
__clean__ : __always__
	$(info [xx] [make] $(@))
	test ! -e $(outputs) || rm -Rf $(outputs)


raster_input_artefact_types := jpeg png

scalable_output_artefact_types := pdf eps svg
raster_output_artefact_types := png
raster_output_artefact_sizes := 240x 320x 480x 640x 800x 1024x
raster_output_artefact_sizetypex := \
		$(foreach output_type, $(raster_output_artefact_types), \
			$(foreach output_size, $(raster_output_artefact_sizes), \
				$(output_size).$(output_type)))


input_artefacts :=
output_artefacts :=
pdf_from_eps_output_artefacts :=
pdf_from_svg_output_artefacts :=
eps_from_dia_output_artefacts :=
eps_from_xmi_output_artefacts :=
eps_from_pdf_output_artefacts :=
svg_from_pdf_output_artefacts :=
png_from_pdf_output_artefacts :=
png_from_png_output_artefacts :=
png_from_jpeg_output_artefacts :=
pdf_from_pdf_output_artefacts :=
svg_from_svg_output_artefacts :=


dia_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.dia' -print))

dia_scalable_output_artefacts := \
		$(foreach output_type, $(scalable_output_artefact_types), \
			$(patsubst %.dia, %.$(output_type), \
				$(filter %.dia, $(dia_input_artefacts))))

dia_raster_output_artefacts := \
		$(foreach output_sizetypex, $(raster_output_artefact_sizetypex), \
			$(patsubst %.dia, %.$(output_sizetypex), \
				$(filter %.dia, $(dia_input_artefacts))))

eps_from_dia_output_artefacts += $(filter %.eps, $(dia_scalable_output_artefacts))
pdf_from_eps_output_artefacts += $(filter %.pdf, $(dia_scalable_output_artefacts))
svg_from_pdf_output_artefacts += $(filter %.svg, $(dia_scalable_output_artefacts))
png_from_pdf_output_artefacts += $(filter %.png, $(dia_raster_output_artefacts))

input_artefacts += $(dia_input_artefacts)
output_artefacts += $(dia_scalable_output_artefacts) $(dia_raster_output_artefacts)


pdf_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.pdf' -not -name '*.loose.pdf' -print))

pdf_loose_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.loose.pdf' -print))

pdf_scalable_output_artefacts := \
		$(foreach output_type, $(filter-out pdf, $(scalable_output_artefact_types)), \
			$(patsubst %.pdf, %.$(output_type), \
				$(filter %.pdf, $(pdf_input_artefacts))) \
			$(patsubst %.loose.pdf, %.$(output_type), \
				$(filter %.loose.pdf, $(pdf_loose_input_artefacts)))) \
		$(patsubst %.loose.pdf, %.pdf, $(pdf_loose_input_artefacts))

pdf_raster_output_artefacts := \
		$(foreach output_sizetypex, $(raster_output_artefact_sizetypex), \
			$(patsubst %.pdf, %.$(output_sizetypex), \
				$(filter %.pdf, $(pdf_input_artefacts))) \
			$(patsubst %.loose.pdf, %.$(output_sizetypex), \
				$(filter %.loose.pdf, $(pdf_loose_input_artefacts))))

eps_from_pdf_output_artefacts += $(filter %.eps, $(pdf_scalable_output_artefacts))
svg_from_pdf_output_artefacts += $(filter %.svg, $(pdf_scalable_output_artefacts))
pdf_from_pdf_output_artefacts += $(filter %.pdf, $(pdf_scalable_output_artefacts))
png_from_pdf_output_artefacts += $(filter %.png, $(pdf_raster_output_artefacts))

input_artefacts += $(pdf_input_artefacts) $(pdf_loose_input_artefacts)
output_artefacts += $(pdf_scalable_output_artefacts) $(pdf_raster_output_artefacts)


svg_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.svg' -not -name '*.loose.svg' -print))

svg_loose_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.loose.svg' -print))

svg_scalable_output_artefacts := \
		$(foreach output_type, $(filter-out svg, $(scalable_output_artefact_types)), \
			$(patsubst %.svg, %.$(output_type), \
				$(filter %.svg, $(svg_input_artefacts))) \
			$(patsubst %.loose.svg, %.$(output_type), \
				$(filter %.loose.svg, $(svg_loose_input_artefacts)))) \
		$(patsubst %.loose.svg, %.svg, $(svg_loose_input_artefacts))

svg_raster_output_artefacts := \
		$(foreach output_sizetypex, $(raster_output_artefact_sizetypex), \
			$(patsubst %.svg, %.$(output_sizetypex), \
				$(filter %.svg, $(svg_input_artefacts))) \
			$(patsubst %.loose.svg, %.$(output_sizetypex), \
				$(filter %.loose.svg, $(svg_loose_input_artefacts))))

eps_from_pdf_output_artefacts += $(filter %.eps, $(svg_scalable_output_artefacts))
pdf_from_svg_output_artefacts += $(filter %.pdf, $(svg_scalable_output_artefacts))
svg_from_svg_output_artefacts += $(filter %.svg, $(svg_scalable_output_artefacts))
png_from_pdf_output_artefacts += $(filter %.png, $(svg_raster_output_artefacts))

input_artefacts += $(svg_input_artefacts) $(svg_loose_input_artefacts)
output_artefacts += $(svg_scalable_output_artefacts) $(svg_raster_output_artefacts)


xmi_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f -name '*.xmi' -print))

xmi_embedded_artefacts := \
		$(foreach input_artefact, $(xmi_input_artefacts), \
			$(addprefix $(input_artefact)-embedded/, \
				$(shell cat $(inputs)/$(input_artefact)-embedded)))

xmi_scalable_output_artefacts := \
		$(foreach output_type, $(scalable_output_artefact_types), \
			$(patsubst %, %.$(output_type), \
				$(xmi_embedded_artefacts)))

xmi_raster_output_artefacts := \
		$(foreach output_sizetypex, $(raster_output_artefact_sizetypex), \
			$(patsubst %, %.$(output_sizetypex), \
				$(xmi_embedded_artefacts)))

eps_from_xmi_output_artefacts += $(filter %.eps, $(xmi_scalable_output_artefacts))
pdf_from_eps_output_artefacts += $(filter %.pdf, $(xmi_scalable_output_artefacts))
svg_from_pdf_output_artefacts += $(filter %.svg, $(xmi_scalable_output_artefacts))
png_from_pdf_output_artefacts += $(filter %.png, $(xmi_raster_output_artefacts))

input_artefacts += $(xmi_input_artefacts)
output_artefacts += $(xmi_scalable_output_artefacts) $(xmi_raster_output_artefacts)


raster_input_artefacts := \
		$(patsubst $(inputs)/%, %, \
			$(shell find $(inputs) \( -path $(outputs) -prune \) -o \( -name '.*' -prune \) -o \( -name 'dia-shapes' -prune \) -o -type f \( \
				$(patsubst %, -name '*.%' -o, $(raster_input_artefact_types)) -false \) -print))

raster_unscaled_output_artefacts := \
		$(foreach input_type, $(raster_input_artefact_types), \
			$(foreach output_type, $(raster_output_artefact_types), \
				$(patsubst %.$(input_type), %.$(output_type), \
					$(filter %.$(input_type), \
						$(filter-out %.$(output_type), $(raster_input_artefacts))))))

raster_scaled_output_artefacts := \
		$(foreach output_sizetypex, $(raster_output_artefact_sizetypex), \
			$(patsubst %, %.$(output_sizetypex), \
				$(foreach input_type, $(raster_input_artefact_types), \
					$(patsubst %.$(input_type), %, \
						$(filter %.$(input_type), \
							$(raster_input_artefacts))))))

png_from_png_output_artefacts += \
		$(foreach output_size, $(raster_output_artefact_sizes), \
			$(patsubst %.png, %.$(output_size).png, \
				$(filter %.png, $(raster_input_artefacts))))

png_from_jpeg_output_artefacts += \
		$(patsubst %.jpeg, %.png, \
			$(filter %.jpeg, $(raster_input_artefacts))) \
		$(foreach output_size, $(raster_output_artefact_sizes), \
			$(patsubst %.jpeg, %.$(output_size).png, \
				$(filter %.jpeg, $(raster_input_artefacts))))

input_artefacts += $(raster_input_artefacts)
output_artefacts += $(raster_unscaled_output_artefacts) $(raster_scaled_output_artefacts)


generate_targets += $(addprefix __generate__@, $(output_artefacts))
view_targets += $(addprefix __view__@, $(input_artefacts) $(output_artefacts))
edit_targets += $(addprefix __edit__@, $(input_artefacts))


.PHONY : $(generate_targets)
.PHONY : $(view_targets)
.PHONY : $(edit_targets)


$(outputs)/.outputs : $(addprefix $(outputs)/, $(input_artefacts) $(output_artefacts))
	touch $(@)

$(outputs)/.publish : $(addprefix $(outputs)/.publish/, $(input_artefacts) $(output_artefacts))
	touch $(@)


$(generate_targets) : __generate__@% : $(outputs)/% __always__
	$(info [xx] [make] $(@))
	test -f $(<)

$(addprefix __view__@, $(filter %.pdf, $(input_artefacts) $(output_artefacts))) : __view__@%.pdf : $(outputs)/%.pdf __always__
	$(info [xx] [make] $(@))
	test -f $(<)
	$(pdf_viewer) $(realpath $(<))

$(addprefix __view__@, $(filter %.eps, $(input_artefacts) $(output_artefacts))) : __view__@%.eps : $(outputs)/%.eps __always__
	$(info [xx] [make] $(@))
	test -f $(<)
	$(eps_viewer) $(realpath $(<))

$(addprefix __view__@, $(filter %.svg, $(input_artefacts) $(output_artefacts))) : __view__@%.svg : $(outputs)/%.svg __always__
	$(info [xx] [make] $(@))
	test -f $(<)
	$(svg_viewer) $(realpath $(<))

$(addprefix __view__@, $(filter %.png, $(input_artefacts) $(output_artefacts))) : __view__@%.png : $(outputs)/%.png __always__
	$(info [xx] [make] $(@))
	test -f $(<)
	$(png_viewer) $(realpath $(<))

$(addprefix __edit__@, $(filter %.dia, $(input_artefacts))) : __edit__@%.dia : __always__
	$(info [xx] [make] $(@))
	test -f $(inputs)/$(*).dia
	dia $(inputs)/$(*).dia

$(addprefix __edit__@, $(filter %.xmi, $(input_artefacts))) : __edit__@%.xmi : __always__
	$(info [xx] [make] $(@))
	test -f $(inputs)/$(*).xmi
	umbrello $(inputs)/$(*).xmi


$(addprefix $(outputs)/, $(eps_from_dia_output_artefacts)) \
		: $(outputs)/%.eps \
		: $(outputs)/%.dia $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	dia -e $(@) -t eps $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(pdf_from_eps_output_artefacts)) \
		: $(outputs)/%.pdf \
		: $(outputs)/%.eps $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	epstopdf $(<) --outfile=$(@) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(svg_from_pdf_output_artefacts)) \
		: $(outputs)/%.svg \
		: $(outputs)/%.pdf $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	inkscape --export-plain-svg $(@) $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(svg_from_svg_output_artefacts)) \
		: $(outputs)/%.svg \
		: $(outputs)/%.loose.svg $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	inkscape --export-plain-svg $(@) --export-area-drawing $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(pdf_from_svg_output_artefacts)) \
		: $(outputs)/%.pdf \
		: $(outputs)/%.svg $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	inkscape --export-pdf $(@) $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(pdf_from_pdf_output_artefacts)) \
		: $(outputs)/%.pdf \
		: $(outputs)/%.loose.pdf $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	inkscape --export-pdf $(@) --export-area-drawing $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(eps_from_pdf_output_artefacts)) \
		: $(outputs)/%.eps \
		: $(outputs)/%.pdf $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	pdftops $(<) -eps $(@) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(addprefix $(outputs)/, $(filter-out $(patsubst %, \%%.png, $(raster_output_artefact_sizes)), $(png_from_jpeg_output_artefacts))) \
		: $(outputs)/%.png \
		: $(outputs)/%.jpeg $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	convert $(<) $(@) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(outputs)/.png_from_png_output_artefacts.mk : $(outputs_exists)
$(outputs)/.png_from_png_output_artefacts.mk : $(patsubst %, $(inputs)/%, $(input_artefacts)) ##
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	for output_size in $(raster_output_artefact_sizes) ; do \
		for output_artefact in $(png_from_png_output_artefacts) ; do \
			case $${output_artefact} in \
				( *$${output_size}.png ) \
					echo ; \
					echo "\$$(outputs)/$${output_artefact} : \$$(outputs)/$${output_artefact%%.$${output_size}.png}.png \$$(outputs_exists)" ; \
					echo "	\$$(info [xx] [make] \$$(patsubst \$$(outputs)/%,%, \$$(@)))" ; \
					echo "	test ! -e \$$(@) || rm -f \$$(@)" ; \
					echo "	convert \$$(<) -resize $${output_size} \$$(@) >\$$(@).log 2>&1 || { cat <\$$(@).log ; exit 1 ; }" ; \
					echo "	test -s \$$(@) || rm -f \$$(@)" ; \
					echo "	test -f \$$(@)" ; \
					echo ; \
				;; \
			esac ; \
		done ; \
	done >$(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)

-include $(outputs)/.png_from_png_output_artefacts.mk

$(addprefix $(outputs)/, $(png_from_png_output_artefacts)) : $(outputs)/.png_from_png_output_artefacts.mk


$(outputs)/.png_from_jpeg_output_artefacts.mk : $(outputs_exists)
$(outputs)/.png_from_jpeg_output_artefacts.mk : $(patsubst %, $(inputs)/%, $(input_artefacts)) ##
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	for output_size in $(raster_output_artefact_sizes) ; do \
		for output_artefact in $(png_from_jpeg_output_artefacts) ; do \
			case $${output_artefact} in \
				( *$${output_size}.png ) \
					echo ; \
					echo "\$$(outputs)/$${output_artefact} : \$$(outputs)/$${output_artefact%%.$${output_size}.png}.jpeg \$$(outputs_exists)" ; \
					echo "	\$$(info [xx] [make] \$$(patsubst \$$(outputs)/%,%, \$$(@)))" ; \
					echo "	test ! -e \$$(@) || rm -f \$$(@)" ; \
					echo "	convert \$$(<) -resize $${output_size} \$$(@) >\$$(@).log 2>&1 || { cat <\$$(@).log ; exit 1 ; }" ; \
					echo "	test -s \$$(@) || rm -f \$$(@)" ; \
					echo "	test -f \$$(@)" ; \
					echo ; \
				;; \
			esac ; \
		done ; \
	done >$(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)

-include $(outputs)/.png_from_jpeg_output_artefacts.mk

$(addprefix $(outputs)/, $(png_from_jpeg_output_artefacts)) : $(outputs)/.png_from_jpeg_output_artefacts.mk


$(outputs)/.png_from_pdf_output_artefacts.mk : $(outputs_exists)
$(outputs)/.png_from_pdf_output_artefacts.mk : $(patsubst %, $(inputs)/%, $(input_artefacts)) ##
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	for output_size in $(raster_output_artefact_sizes) ; do \
		for output_artefact in $(png_from_pdf_output_artefacts) ; do \
			case $${output_artefact} in \
				( *$${output_size}.png ) \
					echo ; \
					echo "\$$(outputs)/$${output_artefact} : \$$(outputs)/$${output_artefact%%.$${output_size}.png}.pdf \$$(outputs_exists)" ; \
					echo "	\$$(info [xx] [make] \$$(patsubst \$$(outputs)/%,%, \$$(@)))" ; \
					echo "	test ! -e \$$(@) || rm -f \$$(@)" ; \
					## echo "	convert -density 72 -resize $${output_size} \$$(<) \$$(@) >\$$(@).log 2>&1 || { cat <\$$(@).log ; exit 1 ; }" ; \
					echo "	inkscape --export-png \$$(@) --export-width $${output_size%%x*} --export-background='#ffffff' \$$(<) >\$$(@).log 2>&1 || { cat <\$$(@).log ; exit 1 ; }" ; \
					echo "	test -s \$$(@) || rm -f \$$(@)" ; \
					echo "	test -f \$$(@)" ; \
					echo ; \
				;; \
			esac ; \
		done ; \
	done >$(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)

-include $(outputs)/.png_from_pdf_output_artefacts.mk

$(addprefix $(outputs)/, $(png_from_pdf_output_artefacts)) : $(outputs)/.png_from_pdf_output_artefacts.mk


$(outputs)/.eps_from_xmi_output_artefacts.mk : $(outputs_exists)
$(outputs)/.eps_from_xmi_output_artefacts.mk : $(patsubst %, $(inputs)/%, $(input_artefacts)) ##
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -f $(@)
	for embedded_artefact in $(xmi_embedded_artefacts) ; do \
		for input_artefact in $(xmi_input_artefacts) ; do \
			case $${embedded_artefact} in \
				( $${input_artefact}-embedded/* ) \
					echo ; \
					echo "\$$(outputs)/$${embedded_artefact}.eps : \$$(outputs)/$${input_artefact}-embedded" ; \
					echo ; \
				;; \
			esac ; \
		done ; \
	done >$(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)

-include $(outputs)/.eps_from_xmi_output_artefacts.mk

$(addprefix $(outputs)/, $(eps_from_xmi_output_artefacts)) : $(outputs)/.eps_from_xmi_output_artefacts.mk

$(patsubst %.xmi, $(outputs)/%.xmi-embedded, $(xmi_input_artefacts)) \
		: $(outputs)/%.xmi-embedded \
		: $(outputs)/%.xmi $(outputs_exists)
	$(info [xx] [make] $(patsubst $(outputs)/%,%, $(@)))
	test ! -e $(@) || rm -Rf $(@)
	mkdir $(@)
	## umbrello --export eps --directory $(@) $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	umbrello --export svg --directory $(@) $(<) >$(@).log 2>&1 || { cat <$(@).log ; exit 1 ; }
	find $(@) -type f -name '*.svg' -print \
	| while read file ; do \
		inkscape --export-eps $${file%%.svg}.eps $${file} ; \
		rm $${file} ; done
	test -n "$$( ls $(@) )" || rm -Rf $(@)
	test -d $(@)


$(patsubst %, $(outputs)/%, $(input_artefacts)) \
		: $(outputs)/% \
		: $(inputs)/% $(outputs_exists)
	test -e $(dir $(@)) || mkdir -p $(dir $(@))
	test ! -e $(@) || rm -f $(@)
	cp -T $(<) $(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)

$(patsubst %, $(outputs)/.publish/%, $(input_artefacts) $(output_artefacts)) \
		: $(outputs)/.publish/% \
		: $(outputs)/% $(outputs_exists)
	test -e $(dir $(@)) || mkdir -p $(dir $(@))
	test ! -e $(@) || rm -f $(@)
	cp -T $(<) $(@)
	test -s $(@) || rm -f $(@)
	test -f $(@)


$(outputs)/.exists :
	test ! -e $(@) || rm -Rf $(@)
	mkdir -p $(outputs)
	touch $(@)
