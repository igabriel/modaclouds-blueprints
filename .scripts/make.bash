#!/bin/bash

set -e -E -u -o pipefail -o noclobber -o noglob +o braceexpand || exit 1
trap 'printf "[ee] failed: %s\n" "${BASH_COMMAND}" >&2' ERR || exit 1

exec </dev/null >/dev/null


_workbench="$( dirname "$( dirname "$( readlink -e "${0}" )" )" )"
_token="$( uuidgen -r )"
_flags=( -R -r -e --warn-undefined-variables )
_variables=( script="${0}" token="${_token}" )
_targets=( )


_continuous=false
_force=false
_silent=true
_debug=false
_fork=8


for _argument in "${@}"
do
	
	if [[ "${_argument}" =~ ^[a-zA-Z0-9_-]+$ ]]
	then
		_targets+=( "${_argument}" )
		
	elif [[ "${_argument}" =~ ^\.?(/[a-zA-Z0-9_.-]+)+/?$ ]]
	then
		_path="$( readlink -m ${_argument} )"
		_targets+=( "${_path}" )
	
	elif [[ "${_argument}" =~ ^__[a-z]+__@([a-zA-Z0-9_.-]+)(/[a-zA-Z0-9_.-]+)+$ ]]
	then
		_targets+=( "${_argument}" )
		
	elif [[ "${_argument}" =~ ^[a-zA-Z0-9_-]+=.*$ ]]
	then
		_variables+=( "${_argument}" )
		
	elif [[ "${_argument}" =~ ^%[a-zA-Z0-9_-]+(=.*)?$ ]]
	then
		case "${_argument}" in
			( %continuous )
				_continuous=true
			;;
			( %force )
				_force=true
			;;
			( %verbose )
				_silent=false
			;;
			( %silent )
				_silent=true
			;;
			( %debug )
				_debug=true
			;;
			( %fork=* )
				_fork="${_argument/#%fork=/}"
			;;
			( * )
				echo "[ee] invalid argument \`${_argument}\`" >&2
				exit 1
			;;
		esac
		
	else
		echo "[ee] invalid argument \`${_argument}\`" >&2
		exit 1
	fi
	
done


if test "${_force}" == true
then
	_flags+=( -B )
fi

if test "${_silent}" == true
then
	_flags+=( -s )
fi

if test "${_debug}" == true
then
	_flags+=( -d )
fi

if test "${_fork}" -gt 0
then
	_flags+=( -j "${_fork}" )
fi


if test "${#_targets[@]}" -eq 0
then
	_targets=( __help__ )
fi


while true
do
	
	_stop=false
	trap '_stop=true' SIGINT SIGTERM SIGQUIT SIGHUP
	
	if test "${_continuous}" == true
	then
		echo '[--]' >&2
		echo '[--] ----------------------------------------' >&2
		echo '[--]' >&2
	fi
	
	for _target in "${_targets[@]}"
	do
		
		nice -n19 \
		env - \
				PATH="${PATH}" \
				USER="${USER}" \
				HOME="${HOME}" \
				TMPDIR="${TMPDIR}" \
				DISPLAY="${DISPLAY}" \
		make \
				-f "${_workbench}/.scripts/make.mk" \
				-C "${_workbench}" \
				"${_flags[@]}" \
				"${_target}" \
				"${_variables[@]}" \
			>&2
		
	done \
	|| test "${_continuous}" == true \
	|| exit 1
	
	test "${_continuous}" == true || break
	test "${_stop}" != true || break
	
	trap 'break' SIGINT SIGTERM SIGQUIT SIGHUP
	sleep 6s
	
done

exit 0
